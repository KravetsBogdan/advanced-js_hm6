"use strict"

// HW6
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

// Асинхронность - это возможность получать ответ не сразу, а через время и затем его обрабатывать. Так как при обращении на сервер нам нужно ждать, пока придет информация.
// Тоесть, результат вернется не сразу, а при формировании до конча

const ipLink = 'https://api.ipify.org/?format=json';
const infoLink = 'http://ip-api.com/json/';
const fields = 'fields=continent,country,regionName,city,district';
const btn = document.querySelector('.find');

class Request {

   async getIp(ipLink) {
      try {
         const response = await fetch(ipLink);
         const { ip } = await response.json()
         if (response.ok) {
            return ip;
         } else {
            throw new Error('Ошибка загрузки данных')
         }

      } catch (error) {
         console.log(error);
      }
   }
   async getInfo(ip) {
      try {
         const response = await fetch(`${infoLink}${ip}?${fields}`);

         if (response.ok) {
            return response.json();
         } else {
            throw new Error('Ошибка загрузки данных')
         }

      } catch (error) {
         console.log(error);
      }
   }
}
class UserIpInfo {

   render(info) {
      try {
         const list = document.createElement('ul');
         Object.keys(info).forEach(key => {
            const item = document.createElement('li');
            item.textContent = `${key.toUpperCase()}: ${info[key]}`;
            list.append(item);
         });
         document.body.append(list);
         return list;

      } catch (err) {
         console.log(err);
      }
   }
}
const user = new UserIpInfo();
const request = new Request();
const handleBtnClick = async () => {
   try {
      const ipData = await request.getIp(ipLink);
      const infoData = await request.getInfo(ipData);
      user.render(infoData);
   } catch (error) {
      console.error(error);
   }
}
btn.addEventListener('click', () => handleBtnClick());

